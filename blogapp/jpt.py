
# def hello(): 
#     print("hello") 
#     print("hello again") 
# hello() 
  
# # calling function 
# hello()
# def hi():
# 	a=10
# 	if(a==10):
# 		print("hi")
# 		for step in range(10):
# 			print("Hello world")
# 	else:	
# 		print("bye") 
# 	print("Ram")

# hi()

# list = ['apple', 'Banana', 1]
# print(list[1])
# list[1]="Mango"
# print(len(list))
# list.append("Jurassic")
# print(list)
# thislist = list(("apple", "banana", "cherry")) # note the double round-brackets
# print(list)
# tuple = ('ram', 'laxman', 'sita')
# # print (tuple[2])
# # for x in tuple:
# # 	print (x)
# del tuple
# print (tuple)

# thisdict= {
# 	"brand" :"Ford",
# 	1: 34,
# 	"Mount":"Everest"
# }
# print (thisdict)
# # z= thisdict[34]
# z = thisdict.get("brand")

# print (z)
# for x in thisdict.values():
# 	print (x)

# for x, y in thisdict.items():
#   print(x, y)
# del thisdict["Mount"]
# print(thisdict)

# i=1
# while i<6:
# 	print(i)
# 	i += 1

# i = 1
# while i < 6:
#   print(i)
#   if i == 3:
#     break
#   i += 1

# def my_function():
# 	print("hello")
# my_function()

# def my_function(fname):
# 	print(fname + "R")

# # my_function("b")

# def my_function(country = "norway"):
# 	print("I"+country)

# my_function("Sweden")

# def my_function(x):
#   return 5 * x

# print(my_function(3))
# x = lambda a : a + 10
# print(x(5))

# def myfunc(n):
#   return lambda a : a * n

# mydoubler = myfunc(2)

# print(mydoubler(11))

# def myfun(n):
# 	return 
from pprint import pprint
import requests
r = requests.get('http://api.openweathermap.org/data/2.5/weather?q=London&APPID={5cfa51d7dcc19f20d6713ec7e37133d2}')
pprint(r.json())
{u'base': u'cmc stations',
 u'clouds': {u'all': 68},
 u'cod': 200,
 u'coord': {u'lat': 51.50853, u'lon': -0.12574},
 u'dt': 1383907026,
 u'id': 2643743,
 u'main': {u'grnd_level': 1007.77,
           u'humidity': 97,
           u'pressure': 1007.77,
           u'sea_level': 1017.97,
           u'temp': 282.241,
           u'temp_max': 282.241,
           u'temp_min': 282.241},
 u'name': u'London',
 u'sys': {u'country': u'GB', u'sunrise': 1383894458, u'sunset': 1383927657},
 u'weather': [{u'description': u'broken clouds',
               u'icon': u'04d',
               u'id': 803,
               u'main': u'Clouds'}],
 u'wind': {u'deg': 158.5, u'speed': 2.36}}