from django.contrib import admin
from .models import Student, Book, Movie, News, Category
# Register your models here.
admin.site.register(Student)
admin.site.register(Book)
admin.site.register(Movie)
admin.site.register(News)
admin.site.register(Category)

