from django.db import models
from django.utils import timezone
# Create your models here.
class Student(models.Model):
	name = models.CharField(max_length=150)
	grade = models.PositiveIntegerField()
	address = models.CharField(max_length=200, null=True, blank=True)
	image = models.ImageField(upload_to="student")
	email = models.EmailField()

	def __str__(self):
		return self.name


class Book(models.Model):
	name = models.CharField(max_length=150)
	author = models.CharField(max_length=150)
	published_date = models.DateField()
	edition = models.PositiveIntegerField()
	image = models.ImageField(upload_to="book")
	issued_by = models.ForeignKey(Student, on_delete = models.SET_NULL, null = True, blank = True)

	def __str__(self):
		return self.name



class Category(models.Model):
	news_category = models.CharField(max_length=100)

	def __str__(self):
		return self.news_category


class News(models.Model):
	title = models.CharField(max_length=500)
	content = models.TextField()
	writer = models.CharField(max_length=50, null=True)
	created_date = models.DateTimeField( default=timezone.now, null=True)
	published_date = models.DateTimeField( blank = False )
	category = models.ForeignKey(Category, on_delete = models.DO_NOTHING, null = True, blank = True)

	news_language = (
		('e', 'English'),
        ('n', 'Nepali'),
	)
	language = models.CharField(
        max_length=20,
        choices=news_language,
        default='e',
    )
	country = models.CharField(max_length=50,blank=True)
	image = models.ImageField(upload_to='news_',null=True)

	def post(self):
		self.created_date = timezone.now()
		self.save() 
		
	def __str__(self):
		#return '%s,%s,%s' %(self.name, self.rating)
		return self.title

# class Staff(models.Model):

industry = (
    	('h','Hollywood'),
    	('b','Bollywood'),
    	('t','Tollywood'),
    	('k','Kollywood'),
    )

class Movie(models.Model):
	name = models.CharField(max_length=150)
	director = models.CharField(max_length=30)
	actor = models.CharField(max_length=100,null=True)
	writer = models.CharField(max_length=50, null=True)
	created_date = models.DateTimeField( default=timezone.now, null=True)
	release_date = models.DateTimeField( blank = False )
	run_time = models.PositiveSmallIntegerField(default=120)
	rating = models.PositiveSmallIntegerField(default=2)
	language = models.CharField(max_length=50,blank=True)
	genre = (
        ('c', 'Comedy'),
        ('t', 'Thriller'),
        ('r', 'Romantic'),
        ('f', 'Fictional'),
        ('n', 'NonFictional'),
        ('h', 'Horror'),

    )
	genre_choices = models.CharField(
        max_length=20,
        choices=genre,
        default='r',
    )
   
	country = models.CharField(
		max_length=10,
		choices=industry,
		default='h',
		)
	poster = models.ImageField(upload_to='poster',null=True)

	def release(self):
		self.release_date = timezone.now()
		self.save() 
		
	def __str__(self):
		#return '%s,%s,%s' %(self.name, self.rating)
		return self.name