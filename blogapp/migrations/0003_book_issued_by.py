# Generated by Django 2.1.3 on 2018-12-31 08:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blogapp', '0002_book'),
    ]

    operations = [
        migrations.AddField(
            model_name='book',
            name='issued_by',
            field=models.ManyToManyField(to='blogapp.Student'),
        ),
    ]
