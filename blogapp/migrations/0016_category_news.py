# Generated by Django 2.1.3 on 2019-01-06 05:50

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('blogapp', '0015_auto_20190101_2030'),
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('news_category', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='News',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=150)),
                ('content', models.TextField()),
                ('writer', models.CharField(max_length=50, null=True)),
                ('created_date', models.DateTimeField(default=django.utils.timezone.now, null=True)),
                ('published_date', models.DateTimeField()),
                ('language', models.CharField(choices=[('e', 'English'), ('n', 'Nepali')], default='e', max_length=20)),
                ('country', models.CharField(blank=True, max_length=50)),
                ('image', models.ImageField(null=True, upload_to='news_')),
                ('category', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='blogapp.Category')),
            ],
        ),
    ]
