from django.urls import path, include
from django.contrib import admin
from django.contrib.auth import authenticate, login
from .views import *
app_name = 'blogapp'


# A primary key of student is passed 
urlpatterns =[
	path('login/', LoginView.as_view(),name='login'),
	path('', HomeView.as_view(),name='home'),
	path('logout/', LogoutView.as_view(),name='logout'),	
	path('movie/', MovieListView.as_view(),name='movie'),
	path('contact/', ContactView.as_view(), name= 'contact'),
	path('about/', AboutView.as_view(), name= 'about' ),
	path('story/', StoryView.as_view(), name= 'story' ),
	path('service/', ServiceView.as_view(), name= 'service' ),
	path('login/', LoginView.as_view(), name = 'login'),
	path('location/', LocationView.as_view(), name = 'location'),
	path('student/list/', StudentListView.as_view(), name='studentlist'),
	path('book/list/', BookListView.as_view(), name='booklist'),
	path('student/<int:pk>/detail/',StudentDetailView.as_view(), name="studentdetail"),
	path('book/<int:pk>/detail/',BookDetailView.as_view(), name="bookdetail"),
	path('movie/list', MovieListView.as_view(), name='movielist'),
	path('movie/<int:pk>/detail/',MovieDetailView.as_view(), name="moviedetail"),
	path('base1/', Base1View.as_view(), name= 'base1' ),
	path('student/add',StudentCreateView.as_view(), name='studentcreate'),
	path('student/<int:pk>/update/',StudentUpdateView.as_view(),name='studentupdate'),
	path('book/add',BookCreateView.as_view(), name='bookcreate'),
	path('book/<int:pk>/update/',BookUpdateView.as_view(),name='bookupdate'),
	path('movie/add',MovieCreateView.as_view(), name='moviecreate'),
	path('movie/<int:pk>/update/',MovieUpdateView.as_view(),name='movieupdate'),
	path('student/<int:pk>/delete/',StudentDeleteView.as_view(),name='studentdelete'),
	path('news/',NewsView.as_view(),name='news'),
	path('search/result/', SearchResultView.as_view(), name = "searchresult")
	# path('movie/<int:pk>/delete/',MovieDeleteView.as_view(),name='moviedelete'),
	#path('book/<int:pk>/delete/',BookDeleteView.as_view(),name='bookdelete'),

]