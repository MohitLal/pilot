from django import forms
from .models import *

class StudentForm(forms.ModelForm):
	class Meta:
		model = Student
		fields = ['name','grade','address','email','image']


class BookForm(forms.ModelForm):
	class Meta:
		model = Book
		fields = ['name','author','edition','published_date','issued_by','image']


class MovieForm(forms.ModelForm):
	class Meta:
		model = Movie
		fields = ['name','director','language','actor','release_date','created_date','rating', 'run_time', 'genre_choices','country','poster']


class LoginForm(forms.Form):
    username = forms.CharField(widget= forms.TextInput(attrs={'class':'form-control'})) #HTML attribute like class of input
    password = forms.CharField(widget= forms.PasswordInput(attrs={'class':'form-control'}))





