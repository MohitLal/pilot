from django.shortcuts import render, redirect
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth import login,logout, authenticate
from django.views.generic import *
from .models import *
import operator
from django.db.models import Q
from django.views.decorators.http import require_http_methods
from .forms import *
# Create your views here.


class HomeView(TemplateView):
	template_name = 'home.html'



class NewsView(ListView):
	template_name = 'news.html'
	context_object_name = 'news'
	queryset = News.objects.filter()
	# queryset = Movie.get_genre_choices_display()


class LoginView(FormView):
	template_name = 'login.html'
	form_class =  LoginForm
	success_url = '/'

	def form_valid(self,form):
		username = form.cleaned_data['username']# username is the form field name 
		password = form.cleaned_data['password']
		user = authenticate(username=username, password=password)# username is the argument of function of authenticate
		if user is not None:
		    login(self.request, user)
		else:
		    return render(self.request,self.template_name,{'form':form, 'errors':'User info did not match' })

		return super().form_valid(form)


class LogoutView(View):
    def get(self, request):
        logout(request)
        return redirect('/')

class ContactView(TemplateView):
	template_name = 'contact.html'


class AboutView(TemplateView):
	template_name = 'about.html'


class Base1View(TemplateView):
	template_name = 'base1.html'


class StoryView(TemplateView):
	template_name = 'story.html'


class ServiceView(TemplateView):
	template_name = 'service.html'


class LocationView(TemplateView):
	template_name = 'location.html'

# A studentlist named context is sent from Student table in student.html file\
class StudentListView(LoginRequiredMixin,ListView):
	login_url = "/login/"
	template_name = 'studentlist.html'  
	queryset = Student.objects.all().order_by('-id')
	context_object_name = "studentlist"


class BookListView(ListView):
	template_name = 'booklist.html'
	model = Book 
	context_object_name = 'booklist'


class StudentDetailView(LoginRequiredMixin, DetailView):
	login_url = '/login/'
	template_name = "studentdetail.html"
	model = Student
	context_object_name = 'student' 


class BookDetailView(DetailView):
	template_name = "bookdetail.html"
	model = Book
	context_object_name = 'book' 


class MovieListView(ListView):
	template_name = 'movielist.html'
	queryset = Movie.objects.all().order_by('-id')
	context_object_name	='movielist'


class MovieDetailView(DetailView):
	template_name = 'moviedetail.html'
	model = Movie
	context_object_name = 'movie'


class StudentCreateView(CreateView):
	template_name = 'studentcreate.html'
	form_class = StudentForm
	success_url = '/'


class StudentUpdateView(UpdateView):
	template_name = 'studentcreate.html'	
	form_class = StudentForm
	model = Student
	success_url = '/'


class StudentDeleteView(DeleteView):
	template_name = 'studentdelete.html'
	model = Student
	success_url = '/student/list'


class BookCreateView(CreateView):
	template_name = 'bookcreate.html'	
	form_class = BookForm
	model = Book
	success_url = '/'


class BookUpdateView(UpdateView):
	template_name = 'bookcreate.html'	
	form_class = BookForm
	model = Book
	success_url = '/'		


class MovieCreateView(CreateView):
	template_name = 'moviecreate.html'	
	form_class = MovieForm
	model = Movie
	success_url = '/'


class MovieUpdateView(UpdateView):
	template_name = 'moviecreate.html'	
	form_class = MovieForm
	model = Movie
	success_url = '/'


class SearchResultView(TemplateView):
	template_name = 'searchresult.html'

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		query = self.request.GET.get('q')
		if query:
			lookup = Q(name__icontains = query)
			slist = Student.objects.filter(lookup)
			context["slist"] = slist
		return context